var sideBar = [
        {
            "name": "All Categories",
            "href": "A1",
            "icono": "glyphicon glyphicon-list",
            "items": { "title": ["Smart Living NEW!", 
                                 "Auto Parts & Accesorries",
                                 "Consumer Electronics",
                                 "Drones & Robotics",
                                 "Fashion Acccessories & Footwear",
                                 "Fashion Apparel & Fabrics",
                                 "Gifts & Premiums",
                                 "Hardware",
                                 "Home Products",
                                 "LED & Solar Products",
                                 "LED & Optoelectronics",
                                 "Machinery & Parts",
                                 "Mobile Electronics",
                                 "Security Products",
                                 "Underwear & Swimwear"
                                ] }
        },
        {
            "name": "Trade Shows",
            "href": "A2",
            "icono": "glyphicon glyphicon-globe",
            "items": { "title": ["Trade Show"] }
        }
        
    ];

var slide = {
    "imagenes": [
        { "items": "img/image1.jpg", "alt": "image1" },
        { "items": "img/image2.jpg", "alt": "image2" },
        { "items": "img/image3.jpg", "alt": "image3" }
    ]
};

function showSideBar() {
    $.each(sideBar, function (i, item) {        
        $('<li><a href="#' + item.href + '" data-toggle="tab"><span class="' + item.icono + '"></span> ' + item.name + '</a></li>').appendTo('.nav.nav-tabs');
        $('<div class="tab-pane" id="' + item.href + '"><ul class="nav nav-stacked' + i + '"></ul></div>').appendTo('.tab-content');
        item.items.title.forEach(function (element, j) {
            
            $('<li><a href="#">' + element + '</a></li>').appendTo('.nav.nav-stacked' + i);
        });
                                          
    });
    $('.tab-pane').first().addClass('active');
    $('.nav.nav-tabs > li').first().addClass('active');
    $('.nav-stacked > li').first().addClass('active');
    $('.nav-tabs > li > a').click(function (event) {
        $(this).addClass('active').siblings().removeClass('active');
    });
    $('.nav-stacked > li').click(function (event) {
        $(this).addClass('active').siblings().removeClass('active');
    })
}

function showCarrousel() {
    $.each(slide.imagenes, function (i, item) {
        $('<div class="item"><img src="' + slide.imagenes[i].items + '"><div class="carousel-caption"></div>   </div>').appendTo('.carousel-inner');
        $('<li data-target="#carousel-example-generic" data-slide-to="' + i + '"></li>').appendTo('.carousel-indicators')
    })
    $('.item').first().addClass('active');
    $('.carousel-indicators > li').first().addClass('active');
    $('#carousel-example-generic').carousel();
}

$(document).ready(function () {
    $('.navbar-nav.first > li').click(function (event) {
        $(this).addClass('active').siblings().removeClass('active');
    });  
    
    showCarrousel()
    showSideBar()
})